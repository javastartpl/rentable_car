public abstract class Vehicle implements Moveable {
    private String name;
    private int year;
    private String direction;

    public Vehicle(String name, int year) {
        this.name = name;
        this.year = year;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    @Override
    public void turnLeft() {
        this.direction = DIRECTION_LEFT;
    }

    @Override
    public void turnRight() {
        this.direction = DIRECTION_RIGHT;
    }

    @Override
    public void goForward() {
        this.direction = DIRECTION_FORWARD;
    }

    @Override
    public void goBack() {
        this.direction = DIRECTION_BACK;
    }

    @Override
    public String toString() {
        return name + " " + year;
    }
}
