public class CarRentalService {
    public static void main(String[] args) {
        Rentable car1 = new RentableCar("Audi A4", 2015, 5);
        car1.rent("Jan", "Kowalski", "123456789");
        System.out.println(car1);
        if(car1.isRent())
            System.out.println("Samochód jest wypożyczony");
        car1.handOver();
        System.out.println(car1);
    }
}
