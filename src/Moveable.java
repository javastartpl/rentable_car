public interface Moveable {
    String DIRECTION_LEFT = "Lewo";
    String DIRECTION_RIGHT = "Prawo";
    String DIRECTION_FORWARD = "Prosto";
    String DIRECTION_BACK = "Wstecz";

    void turnLeft();
    void turnRight();
    void goForward();
    void goBack();
}
